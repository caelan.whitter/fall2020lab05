// Caelan Whitter 1841768

package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		
		String srcDir = "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testInput";
		String desDir ="C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutput";
		String desDir2= "C:\\Users\\Caelan\\Documents\\thirdsemester\\programming\\testOutput2";
		LowercaseProcessor obj = new LowercaseProcessor(srcDir,desDir);
		obj.execute();
		
		RemoveDuplicates obj2 = new RemoveDuplicates(desDir,desDir2);
		obj2.execute();
		
	}

}
