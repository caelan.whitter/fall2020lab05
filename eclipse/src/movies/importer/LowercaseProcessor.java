// Caelan Whitter 1841768
package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor(String srcDir,String desDir)
	{
		super(srcDir,desDir,true);
	}
	
	public ArrayList<String> process(ArrayList<String> words) 
	{
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (int i = 0 ; i < words.size(); i++)
		{
			String newWord = words.get(i).toLowerCase();
			asLower.add(newWord);
		}
		
		
		
		return asLower;
	}

}


