// Caelan Whitter 1841768
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates(String srcDir,String desDir)
	{
		super(srcDir,desDir,false);
	}
	
	public ArrayList<String> process(ArrayList<String> words) 
	{
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (int i = 0 ; i < words.size(); i++)
		{
			String newWord = words.get(i);
			if(asLower.contains(newWord))
			{
				continue;
			}
			
			asLower.add(newWord);
		}
		
		return asLower;
	}

}
